#!/bin/bash

printenv -0 | while IFS= read -r -d '' line; do
    if [[ $line != TTRSS_* ]]; then
        continue
    fi
    VARNAME=$( echo $line | cut -d '=' -f 1 | sed 's/^TTRSS_//' )
    VARVALUE=$( echo $line | cut -d '=' -f 2- )
    sed -i "s|\(define('${VARNAME}'\).\+);|\1, \"$VARVALUE\");|" /var/www/config.php
done

exec "$@"
