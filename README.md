# Tiny Tine RSS Docker image

This is my attempt to create a [ttrss](https://tt-rss.org) Docker image. It is based on [Ubuntu 18.04](https://hub.docker.com/_/ubuntu/)
and contains everything you'll need to run it, except for the database.

## Running

Since ttrss requires three daemons to run (PHP-FPM, Nginx and feed-updates), three containes have to be created. As it was said above,
the image is universal and has to be started three times with different `CMD`'s. This repo contains a [sample docker-compose file](./docker-compose.yml)
that you can use as a base for you own service. The only parameter that you probably want to configure will be `TTRSS_SELF_URL_PATH`. To override defaults,
one can create a [docker-compose.override.yml file](https://docs.docker.com/compose/extends/#multiple-compose-files):

```yaml
services:
  fpm:
    environment:
      TTRSS_SELF_URL_PATH: https://my-ttrss.domain.tld
      TTRSS_DB_PASS: ImpossibleToGuess
  updater:
    environment:
      TTRSS_SELF_URL_PATH: https://my-ttrss.domain.tld
      TTRSS_DB_PASS: ImpossibleToGuess
  postgres:
    environment:
      POSTGRES_PASSWORD: ImpossibleToGuess
```

## Configuration

The ttrss image is configured with environment variables starting with `TTRSS_`. The rest of the variable's name is matched against
[config.php](https://tt-rss.org/gitlab/fox/tt-rss/blob/master/config.php-dist) constants. The most notable variables would be `TTRSS_DB_HOST`, `TTRSS_DB_NAME`, `TTRSS_DB_USER`,
`TTRSS_DB_PASS` and `TTRSS_SELF_URL_PATH`.


## DB initialisation

To initialise PostgreSQL database, download [the schema](https://tt-rss.org/gitlab/fox/tt-rss/blob/master/schema/ttrss_schema_pgsql.sql) and run the following command:

    cat ttrss_schema_pgsql.sql | docker -i dockerttrss_postgres_1 --user postgres psql -U ttrss ttrss

If you already have a dump from your currently running ttrss installation, run this (assuming that the dump was made with the `-Fc` option:

    cat ttrss.dump | docker exec --user postgres -i dockerttrss_postgres_1 pg_restore -O -x -d ttrss -U ttrss
